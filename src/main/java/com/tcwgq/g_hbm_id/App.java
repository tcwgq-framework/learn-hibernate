package com.tcwgq.g_hbm_id;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure().addClass(User.class)
				.buildSessionFactory();
	}

	@Test
	public void testSave() {
		User user = new User();
		// user.setId(1);
		user.setName("张三");
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(user);
//		session.save(new User(null, "李四"));
		// 获取当前session中的事务
		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void testGet() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		User user = (User) session.get(User.class, 1);
		ts.commit();
		session.close();
		System.out.println(user);
	}
}
