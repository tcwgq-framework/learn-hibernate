package com.tcwgq.e_test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;
import com.tcwgq.a_domain.User;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		sessionFactory = cfg.buildSessionFactory();
	}

	@Test
	public void testSave() {
		User user = new User();
		user.setName("张三");
		// 保存到数据库
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();// 开始事务
		session.save(user);
		ts.commit();
		session.close();// 关闭session，释放资源，不一定是真正的关闭，内部使用缓冲池技术
	}

	@Test
	public void testGet() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		User user = (User) session.get(User.class, 1);
		ts.commit();
		session.close();
		System.out.println(user);
	}
}
