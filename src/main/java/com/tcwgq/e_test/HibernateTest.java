package com.tcwgq.e_test;

import java.util.List;
import org.junit.Test;
import com.tcwgq.a_domain.QueryResult;
import com.tcwgq.a_domain.User;
import com.tcwgq.b_dao.UserDao;
import com.tcwgq.c_daoImpl.UserDaoHibernateImpl;

public class HibernateTest {
	private UserDao userDao = new UserDaoHibernateImpl();

	@Test
	public void testInsert() {
		for (int i = 0; i < 25; i++) {
			User user = new User();
			user.setName("test_" + i);
			userDao.save(user);
		}
	}

	@Test
	public void testSave() {
		User user = new User();
		user.setName("张三");
		userDao.save(user);
	}

	@Test
	public void testUpdate() {
		User user = userDao.getById(1);
		user.setName("李四");
		userDao.update(user);
	}

	@Test
	public void testDelete() {
		userDao.delete(1);
	}

	@Test
	public void testGetById() {
		User user = userDao.getById(1);
		System.out.println(user);
	}

	@Test
	public void testFindAll() {
		List<User> list = userDao.findAll();
		for (User user : list) {
			System.out.println(user);
		}
	}

	@Test
	public void testFindAllIntInt() {
		// Page p = userDao.findAll(0, 10);
		// Page p = userDao.findAll(10, 10);
		QueryResult rs = userDao.findAll(20, 10);
		int count = rs.getCounts();
		List<User> list = rs.getList();
		System.out.println(count);
		for (User user : list) {
			System.out.println(user);
		}
	}

}
