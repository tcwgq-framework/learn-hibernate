package com.tcwgq.n_hbm_extends;

public class Topic extends Article {
	private int type;// 表示帖子类型，例如精华，置顶等

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
