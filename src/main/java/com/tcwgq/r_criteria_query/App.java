package com.tcwgq.r_criteria_query;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure()
				.addClass(Department.class).addClass(Employee.class)
				.buildSessionFactory();
	}

	@Test
	public void testSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		for (int i = 1; i <= 10; i++) {
			Department department = new Department();
			department.setName("dept_" + i);
			session.save(department);
		}
		for (int j = 1; j <= 20; j++) {
			Employee employee = new Employee();
			employee.setName("emp_" + j);
			session.save(employee);
		}
		session.getTransaction().commit();
		session.close();
	}

	// QBC query by criteria
	@Test
	public void testQBC() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		Criteria criteria = session.createCriteria(Employee.class);
		// 添加过滤条件
		criteria.add(Restrictions.ge("id", 1));
		criteria.add(Restrictions.le("id", 8));
		// 添加排序条件
		criteria.addOrder(Order.desc("id"));
		criteria.addOrder(Order.asc("name"));
		// 执行查询
		// criteria.setFirstResult(0);
		// criteria.setMaxResults(10);
		// criteria.uniqueResult();
		// 显示结果
		List<?> list = criteria.list();
		for (Object obj : list) {
			if (obj.getClass().isArray()) {
				System.out.println(Arrays.toString((Object[]) obj));
			} else {
				System.out.println(obj);
			}
		}
		ts.commit();
		session.close();
	}

}
