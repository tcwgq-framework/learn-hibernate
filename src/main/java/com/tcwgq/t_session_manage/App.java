package com.tcwgq.t_session_manage;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure(
				"com/tcwgq/t_session_manage/Myhibernate.cfg.xml")
				.buildSessionFactory();
	}

	@Test
	public void testSession() {
		Session session1 = sessionFactory.getCurrentSession();
		Session session2 = sessionFactory.getCurrentSession();
		System.out.println(session1 != null);
		System.out.println(session1 == session2);
	}

	@Test
	public void testSession2() {
		Session session1 = sessionFactory.openSession();
		Session session2 = sessionFactory.openSession();
		System.out.println(session1 != null);
		System.out.println(session1 == session2);
	}

	@Test
	public void testSessionClose() {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		System.out.println("--------");
		session.getTransaction().commit();
		session.close();
	}
}
