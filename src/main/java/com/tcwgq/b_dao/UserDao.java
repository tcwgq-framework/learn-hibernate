package com.tcwgq.b_dao;

import com.tcwgq.a_domain.QueryResult;
import com.tcwgq.a_domain.User;

import java.util.List;

public interface UserDao {
    void save(User user);

    void update(User user);

    void delete(int id);

    User getById(int id);

    List<User> findAll();

    // 分页查询
    QueryResult findAll(int begin, int pageSize);
}
