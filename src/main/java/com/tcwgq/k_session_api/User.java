package com.tcwgq.k_session_api;

public class User {
	private Integer id;
	private String name;

	// 用于测试session中的数据对象过多，造成内存溢出
	// private byte[] bys = new byte[1024 * 1024 * 100];

	public User() {
		super();
	}

	public User(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + "]";
	}

}
