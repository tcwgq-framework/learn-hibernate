package com.tcwgq.f_hbm_property;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure().addClass(User.class)
				.buildSessionFactory();
	}

	@Test
	public void testSave() throws IOException {
		User user = new User();
		user.setName("张三");
		user.setAge(23);
		user.setBirthday(new Date());
		user.setDesc("我是张三，在家排行老三");
		InputStream is = new FileInputStream("hibernate_logo.gif");
		byte[] bys = new byte[is.available()];
		is.read(bys);
		is.close();
		user.setPhoto(bys);
		// 保存到数据库
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();// 开始事务
		session.save(user);
		ts.commit();
		session.close();// 关闭session，释放资源，不一定是真正的关闭，内部使用缓冲池技术
	}

	@Test
	public void testGet() throws IOException {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		User user = (User) session.get(User.class, 1);
		ts.commit();
		session.close();
		System.out.println(user);
		OutputStream os = new FileOutputStream("copy.gif");
		os.write(user.getPhoto());
		os.close();
	}
}
