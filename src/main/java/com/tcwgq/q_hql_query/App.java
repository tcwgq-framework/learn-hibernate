package com.tcwgq.q_hql_query;

import java.util.Arrays;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

public class App {
	private static SessionFactory sessionFactory = null;
	static {
		sessionFactory = new Configuration().configure()
				.addClass(Department.class).addClass(Employee.class)
				.buildSessionFactory();
	}

	@Test
	public void testSave() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		for (int i = 1; i <= 10; i++) {
			Department department = new Department();
			department.setName("dept_" + i);
			session.save(department);
		}
		for (int j = 1; j <= 20; j++) {
			Employee employee = new Employee();
			employee.setName("emp_" + j);
			session.save(employee);
		}
		session.getTransaction().commit();
		session.close();
	}

	// 使用HQL进行查询
	// sql语句查询的是表和表中的列，hql查询的是对象和对象的属性
	// select * 可以省略
	@Test
	public void testHqlQuery1() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		String hql = null;
		// 1.简单的查询条件
		// hql = "from Employee";//注意区分的类名的大小写
		// hql = "from Employee as emp";// 可以使用别名
		// 2.带过滤条件的查询
		// hql = "from Employee where id < 10";// 不带表的别名
		// hql = "from Employee as emp where emp.id <= 10";// 带表的别名，as可以省略
		// 3.带上排序条件的：order by
		// hql = "from Employee where id <= 10 order by id desc, name asc";
		// 5.指定select子句(不可以使用select *)
		// hql = "select emp from Employee emp";// 相当于from Employeee，查询所有记录
		// hql = "select emp.id from Employee emp";// 查询一个列，返回的集合中的数据类型就是列的数据类型
		// hql = "select emp.id, emp.name from Employee emp";//
		// 查询多个列，返回的集合中的元素类型是Object数组，使用不是很方便
		// 6.可以使用new语法，指定把查询出的部分属性封装到对象中
		// hql = "select new Employee(emp.id, emp.name) from Employee emp";
		// 7.执行查询，获得结果(list, uniqueResult, 分页)
		hql = "from Employee where id < 5";
		Query query = session.createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(10);
		List<?> list = query.list();
		// 8.方法链
		// Employee employee = (Employee) session//
		// .createQuery(//
		// "from Employee emp where id < 3"//
		// )//
		// .setFirstResult(0)//
		// .setMaxResults(10)//
		// .uniqueResult();// 查询的结果唯一，当结果不唯一，就会抛异常
		// System.out.println(employee);
		for (Object obj : list) {
			if (obj.getClass().isArray()) {
				System.out.println(Arrays.toString((Object[]) obj));
			} else {
				System.out.println(obj);
			}
		}
		ts.commit();
		session.close();
	}

	@Test
	public void testHqlQuery2() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		String hql = null;

		// 1.聚集函数 ，count(*)，min，max，avg等
		// hql = "select count(*) from Employee";
		// hql = "select max(id) from Employee";
		// Number result = (Number) session.createQuery(hql).uniqueResult();
		// System.out.println(result.getClass());
		// System.out.println(result);
		// 2.分组：group by...having.....
		// 按照name进行分组，并统计每个分组中成员的个数
		// hql =
		// "select emp.name, count(emp.id) from Employee as emp where id < 10 group by emp.name";
		// 显示成员数大于1的分组
		// hql =
		// "select emp.name, count(emp.id) from Employee as emp group by emp.name having count(emp.id) > 1";
		// 按分组成员数降序或者升序排列
		// 在group by子句中不能使用列别名，在order by子句中可以使用列别名的
		// hql =
		// "select emp.name, count(emp.id) from Employee as emp group by emp.name having count(emp.id) > 1 order by count(emp.id) desc";
		// 3.连接查询/HQL是面向对象的查询
		// 内连接，inner关键字可以省略
		// hql =
		// "select emp.id, emp.name, dept.name from Employee emp inner join emp.department dept";
		// 外连接，分为左外连接和右外连接，outer关键字可以省略
		// 左外连接
		// hql =
		// "select emp.id, emp.name, dept.name from Employee emp LEFT join emp.department dept";
		// 右外连接
		// hql =
		// "select emp.id, emp.name, dept.name from Employee emp RIGHT join emp.department dept";
		// 可以使用更为方便的方法
		// hql =
		// "select emp.id, emp.name, emp.department.name from Employee emp";
		// 4.查询时使用参数
		// 一，使用?占位符
		// hql = "select emp.id, emp.name from Employee emp where id = ?";
		// List<?> list = session.createQuery(hql).setParameter(0, 3).list();
		// 二.使用变量名
		// hql =
		// "select emp.id, emp.name from Employee emp where id between :min and :max";
		// List<?> list = session.createQuery(hql).setParameter("min", 3)
		// .setParameter("max", 10).list();
		// 当参数是集合时，一定要使用setParameterList()设置参数值
		// hql = "select emp.id, emp.name from Employee emp where id in (:ids)";
		// List<?> list = session.createQuery(hql)
		// .setParameterList("ids", new Object[] { 1, 3, 5, 7 }).list();
		// List<?> list = session.createQuery(hql).list();
		// 5.使用命名查询
		// Query query = session.getNamedQuery("queryByIdRange");
		// List<?> list = query.setParameter("minId", 3).setParameter("maxId",
		// 10)
		// .list();
		// 6.update与delete，不会通知session缓存
		// 更新
		// int count = session
		// .createQuery(
		// "update Employee emp set emp.name = ? where emp.id > ?")
		// .setParameter(0, "hahaha").setParameter(1, 12).executeUpdate();
		// System.out.println(count);
		// sql语句中的from可以省略
		// 删除
		int count = session.createQuery("delete Employee emp where emp.id > ?")
				.setParameter(0, 12).executeUpdate();
		System.out.println(count);
		// for (Object obj : list) {
		// if (obj.getClass().isArray()) {
		// System.out.println(Arrays.toString((Object[]) obj));
		// } else {
		// System.out.println(obj);
		// }
		// }
		ts.commit();
		session.close();
	}

	// update不通知session缓存，设置事务的隔离级别可以实现更新之后立马显示更新
	@Test
	public void testHQL_DML() {
		Session session = sessionFactory.openSession();
		Transaction ts = session.beginTransaction();
		Employee emp = (Employee) session.get(Employee.class, 10);
		System.out.println(emp.getName());
		int count = session
				.createQuery(
						"update Employee emp set emp.name = ? where emp.id > ?")
				.setParameter(0, "HaHaHa").setParameter(1, 9).executeUpdate();
		System.out.println(count);
		System.out.println(emp.getName());
		ts.commit();
		session.close();
	}

}
