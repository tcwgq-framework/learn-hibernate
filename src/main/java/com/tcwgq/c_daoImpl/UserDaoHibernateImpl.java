package com.tcwgq.c_daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.tcwgq.a_domain.QueryResult;
import com.tcwgq.a_domain.User;
import com.tcwgq.b_dao.UserDao;
import com.tcwgq.d_utils.HibernateUtils;

public class UserDaoHibernateImpl implements UserDao {
	public void save(User user) {
		Session session = HibernateUtils.openSession();
		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			session.save(user);
			ts.commit();
		} catch (RuntimeException e) {
			ts.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public void update(User user) {
		Session session = HibernateUtils.openSession();
		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			session.update(user);
			ts.commit();
		} catch (RuntimeException e) {
			ts.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public void delete(int id) {
		Session session = HibernateUtils.openSession();
		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			Object user = session.get(User.class, id);
			session.delete(user);
			ts.commit();
		} catch (RuntimeException e) {
			ts.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public User getById(int id) {
		Session session = HibernateUtils.openSession();
		Transaction ts = null;
		User user = null;
		try {
			ts = session.beginTransaction();
			user = (User) session.get(User.class, id);
			ts.commit();
			return user;
		} catch (RuntimeException e) {
			ts.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public List<User> findAll() {
		Session session = HibernateUtils.openSession();
		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			// 使用HQL语句查询
			@SuppressWarnings("unchecked")
			// 方式一，使用hql语句查询
			// List<User> list = session.createQuery("FROM User").list();
			// 方式二，使用creteria查询
			Criteria criteria = session.createCriteria(User.class);
			// criteria.add(Restrictions.eq("id", 1));// 添加限制条件
			// criteria.addOrder(Order.asc("id"));
			List<User> list = criteria.list();
			ts.commit();
			return list;
		} catch (RuntimeException e) {
			ts.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public QueryResult findAll(int begin, int pageSize) {
		Session session = HibernateUtils.openSession();
		Transaction ts = null;
		try {
			ts = session.beginTransaction();
			// Query query = session.createQuery("FROM User");
			// query.setFirstResult(begin);
			// query.setMaxResults(pageSize);
			// @SuppressWarnings("unchecked")
			// List<User> list = query.list();
			@SuppressWarnings("unchecked")
			List<User> list = session.createQuery("FROM User")//
					.setFirstResult(begin)//
					.setMaxResults(pageSize).list();//
			// long counts = (Long)
			// session.createQuery("Select count(*) from User")
			// .list().get(0);
			// 下面需要注意，要使用long的包装类
			Long counts = (Long) session.createQuery(
					"select count(*) from User").uniqueResult();
			ts.commit();
			return new QueryResult(counts.intValue(), list);
		} catch (RuntimeException e) {
			ts.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

}
